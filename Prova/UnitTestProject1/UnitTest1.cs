﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Prova;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            MyLib lib = new MyLib();
            Assert.AreEqual(lib.M(10), 20);
        }
    }
}
