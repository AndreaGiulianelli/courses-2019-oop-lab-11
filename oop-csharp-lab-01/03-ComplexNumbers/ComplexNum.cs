﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }


        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return Math.Sqrt(Math.Pow(this.Re, 2) * Math.Pow(this.Im, 2));
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(this.Re, -this.Im);
            }
        }


        public static ComplexNum operator+(ComplexNum x1, ComplexNum x2)
        {
            return new ComplexNum(x1.Re + x2.Re, x1.Im + x2.Im);
        }

        public static ComplexNum operator-(ComplexNum x1, ComplexNum x2)
        {
            return new ComplexNum(x1.Re - x2.Re, x1.Im - x2.Im);
        }

        public static ComplexNum operator*(ComplexNum x1, ComplexNum x2)
        {
            return new ComplexNum(x1.Re * x2.Re - x1.Im * x2.Im, x1.Re * x2.Im + x1.Im * x2.Re); 
        }

        public static ComplexNum operator/(ComplexNum x1, ComplexNum x2)
        {
            double denominatore = Math.Pow(x2.Re, 2) + Math.Pow(x2.Im, 2);
            return new ComplexNum((x1.Re * x2.Re + x1.Im * x2.Im)/(denominatore), (x1.Im * x2.Re - x1.Re * x2.Im)/(denominatore));
        }

        public void Invert()
        {
            double denominatore = Math.Pow(this.Module, 2);
            this.Re = this.Conjugate.Re / denominatore;
            this.Im = this.Conjugate.Im / denominatore;
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            return this.Im > 0 ? $"{this.Re} +{this.Im}i": $"{this.Re} {this.Im}i";
        }


    }
}
