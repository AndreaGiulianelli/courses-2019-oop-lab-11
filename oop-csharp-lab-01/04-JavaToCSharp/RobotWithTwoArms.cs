﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.arms
{
    class RobotWithTwoArms : BaseRobot, RobotWithArms
    {
        public const double TRANSPORT_OBJECT_CONSUMPTION = 0.1;

        private readonly BasicArm leftArm;
        private readonly BasicArm rightArm;

        public RobotWithTwoArms(string robotName) : base(robotName)
        {
            leftArm = new BasicArm("Left arm");
            rightArm = new BasicArm("Right arm");
        }

        protected override double getBatteryRequirementForMovement()
        {
            return base.getBatteryRequirementForMovement() + getCarriedItemsCount() * TRANSPORT_OBJECT_CONSUMPTION;
        }

        private void doPick(BasicArm arm)
        {
            if (isBatteryEnough(arm.getConsuptionForPickUp()) && !arm.isGrabbing())
            {
                log(arm + " is picking an object");
                arm.pickUp();
                consumeBattery(arm.getConsuptionForPickUp());
            }
            else
            {
                log("Can not grab (battery=" + this.Battery + "," + arm + " isGrabbing=" + arm.isGrabbing() + ")");
            }
        }

        private void doRelease(BasicArm arm)
        {
            if (isBatteryEnough(arm.getConsuptionForDropDown()) && arm.isGrabbing())
            {
                this.log(arm + " is releasing an object");
                arm.dropDown();
                this.consumeBattery(arm.getConsuptionForDropDown());
            }
            else
            {
                log("Can not release (batteryLevel=" + this.Battery + "," + arm + " isGrabbing="
                        + arm.isGrabbing() + ")");
            }
        }

        public bool dropDown()
        {
            if (leftArm.isGrabbing())
            {
                doRelease(leftArm);
                return true;
            }
            if (rightArm.isGrabbing())
            {
                doRelease(rightArm);
                return true;
            }
            return false;
        }

        public int getCarriedItemsCount()
        {
            return (leftArm.isGrabbing() ? 1 : 0) + (rightArm.isGrabbing() ? 1 : 0);
        }

        public bool pickUp()
        {
            if (rightArm.isGrabbing())
            {
                if (leftArm.isGrabbing())
                {
                    return false;
                }
                doPick(leftArm);
            }
            else
            {
                doPick(rightArm);
            }
            return true;
        }
    }
}
