﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.bas
{
    public interface Position2D
    {
        int X { get; set; }

        int Y { get; set; }

        Position2D sumVector(Position2D p);

        Position2D sumVector(int x, int y);
    }
}
