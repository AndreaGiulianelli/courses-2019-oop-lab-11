﻿using _04_JavaToCSharp.robot.bas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.arms
{
    public interface RobotWithArms : Robot
    {
        bool pickUp();

        bool dropDown();

        int getCarriedItemsCount();
    }
}
