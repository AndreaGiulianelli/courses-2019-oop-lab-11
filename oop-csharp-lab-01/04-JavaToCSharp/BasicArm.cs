﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.arms
{
    public class BasicArm
    {
        const double ENERGY_REQUIRED_TO_MOVE = 0.2;
        const double ENERGY_REQUIRED_TO_GRAB = 0.1;

        bool grabbing;

        private String Name { get; set; }

        public BasicArm(String name)
        {
            this.Name = name;
        }

        public bool isGrabbing()
        {
            return this.grabbing;
        }

        public void pickUp()
        {
            grabbing = true;
        }

        public void dropDown()
        {
            grabbing = false;
        }

        public double getConsuptionForPickUp()
        {
            return ENERGY_REQUIRED_TO_MOVE + ENERGY_REQUIRED_TO_GRAB;
        }

        public double getConsuptionForDropDown()
        {
            return ENERGY_REQUIRED_TO_MOVE;
        }

        public String toString()
        {
            return this.Name;
        }
    }

}
