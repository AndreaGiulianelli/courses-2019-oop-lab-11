﻿using _04_JavaToCSharp.robot.bas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp
{
    public class RobotPosition : Position2D
    {

        public int X { get; set; }
        public int Y { get; set; }

        public RobotPosition(int x, int y)
        {
            X = x;
            Y = y;
        }


        public Position2D sumVector(Position2D p)
        {
            return new RobotPosition(X + p.X, Y + p.Y);
        }

        public Position2D sumVector(int x, int y)
        {
            return new RobotPosition(this.X + x, this.Y + y);
        }

        public override bool Equals(Object o)
        {
            if (o is Position2D) {
                var p = (Position2D)o;
                return X == p.X && Y == p.Y;
            }
            return false;
        }

        public int hashCode()
        {
            return X ^ Y;
        }

        public String toString()
        {
            return "[" + X + ", " + Y + "]";
        }
    }
}
