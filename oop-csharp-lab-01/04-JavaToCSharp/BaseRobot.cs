﻿using _04_JavaToCSharp.robot.bas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp
{
    public class BaseRobot : Robot
    {

        public const double BATTERY_FULL = 100;
        public const double MOVEMENT_DELTA_CONSUMPTION = 1.2;
        private const int MOVEMENT_DELTA = 1;

        private readonly RobotEnvironment environment;
        private readonly String robotName;
        private double batteryLevel;

        public BaseRobot(String robotName)
        {
            this.robotName = robotName;
            this.environment = new RobotEnvironment(new RobotPosition(0, 0));
            this.batteryLevel = BATTERY_FULL;
        }

        public double Battery
        {
            get
            {
                return Math.Round(batteryLevel * 100d) / BATTERY_FULL;
            }
        }

        public Position2D Position
        {
            get
            {
                return this.environment.Position;
            }
        }

        protected virtual void consumeBattery(double amount)
        {
            if(batteryLevel >= amount)
            {
                batteryLevel -= amount;
            }
            else
            {
                batteryLevel = 0;
            }
        }

        void consumeBatteryForMovement()
        {
            consumeBattery(getBatteryRequirementForMovement());
        }

        protected virtual double getBatteryRequirementForMovement()
        {
            return MOVEMENT_DELTA_CONSUMPTION;
        }

        protected virtual bool isBatteryEnough(double operationCost)
        {
            return batteryLevel > operationCost;
        }

        protected void log(String msg)
        {
            Console.WriteLine("[" + this.robotName + "]: " + msg);
        }

        private bool move(int dx,int dy)
        {
            if (isBatteryEnough(getBatteryRequirementForMovement()))
            {
                if (environment.move(dx, dy))
                {
                    consumeBatteryForMovement();
                    log("Moved to position " + environment.Position + ". Battery: " + Battery + "%.");
                    return true;
                }
                log("Can not move of (" + dx + "," + dy
                        + ") the robot is touching the world boundary: current position is " + environment.Position);
            }
            else
            {
                log("Can not move, not enough battery. Required: " + getBatteryRequirementForMovement()
                    + ", available: " + batteryLevel + " (" + Battery + "%)");
            }
            return false;
        }

        public bool moveDown()
        {
            return move(0, -MOVEMENT_DELTA);
        }

        public bool moveLeft()
        {
            return move(-MOVEMENT_DELTA, 0);
        }

        public bool moveRight()
        {
            return move(MOVEMENT_DELTA, 0);
        }

        public bool moveUp()
        {
            return move(0, MOVEMENT_DELTA);
        }

        public void recharge()
        {
            batteryLevel = BATTERY_FULL;
        }

        public String toString()
        {
            return robotName;
        }
    }
}
