﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    class FrenchDeck
    {
        private Card[] cards;

        public FrenchDeck()
        {
            cards = new Card[Enum.GetNames(typeof(FrenchSeed)).Length * Enum.GetNames(typeof(FrenchValue)).Length + 2];
        }

        public Card this[FrenchSeed seed, FrenchValue val]
        {
            get
            {
                foreach (Card c in cards)
                {
                    if (c.Seed == seed.ToString() && c.Value == val.ToString())
                    {
                        return c;
                    }
                }
                return null;
            }
            set
            {
                for (int i = 0; i < cards.Length; i++)
                {
                    if (cards[i] == null || cards[i].Seed == seed.ToString() && cards[i].Value == val.ToString())
                    {
                        cards[i] = value;
                        break;
                    }
                }
            }
        }

        public Card this[FrenchJolly jolly]
        {
            get
            {
                foreach (Card c in cards)
                {
                    if (c.Seed == jolly.ToString() && c.Value == null)
                    {
                        return c;
                    }
                }
                return null;
            }
            set
            {
                for (int i = 0; i < cards.Length; i++)
                {
                    if (cards[i] == null || cards[i].Seed == jolly.ToString() && cards[i].Value == null)
                    {
                        cards[i] = value;
                        break;
                    }
                }
            }
        }

        public void Initialize()
        {
            foreach (FrenchSeed seed in (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed)))
            {
                foreach (FrenchValue value in (FrenchValue[])Enum.GetValues(typeof(ItalianValue)))
                {
                    Card x = new Card(value.ToString(), seed.ToString());
                    this[seed, value] = x;
                }
            }

            foreach (FrenchJolly jolly in (FrenchJolly[])Enum.GetValues(typeof(FrenchJolly)))
            {
                Card x = new Card(null, jolly.ToString());
                this[jolly] = x;
            }
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            foreach (Card x in cards)
            {
                Console.WriteLine(x);
            }
        }

    }

    enum FrenchSeed
    {
        DENARI,
        COPPE,
        SPADE,
        BASTONI
    }

    enum FrenchJolly
    {
        JOLLY1,
        JOLLY2
    }

    enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
