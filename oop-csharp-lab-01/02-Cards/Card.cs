﻿namespace Cards
{
    class Card
    {
        public Card(string value, string seed)
        {
            Value = value;
            Seed = seed;
        }
        /*
        public string GetSeed()
        {
            return this.seed;
        }

        public string GetValue()
        {
            return this.value;
        }
        */

        public string Seed { get; private set; }
        public string Value { get; private set; }

        public override string ToString()
        {
            // TODO comprendere il meccanismo denominato in C# "string interpolation"
            return $"{this.GetType().Name}(Name={Value}, Seed={Seed})";
        }
    }

    
}
