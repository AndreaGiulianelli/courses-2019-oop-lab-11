﻿using _04_JavaToCSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_JavaToCSharpTest
{
    [TestClass]
    public class TestRobots
    {
        [TestMethod]
        public void Test1()
        {
            Console.WriteLine("A");
            var stepsDefault = (int)(BaseRobot.BATTERY_FULL / BaseRobot.MOVEMENT_DELTA_CONSUMPTION);
            var r0 = new BaseRobot("R2D2");
            var r0pos = r0 + " position";
            var r0bat = r0 + " battery";
            RobotPosition x = new RobotPosition(0, 0);
            Assert.AreEqual(x, r0.Position);
            Assert.AreEqual(BaseRobot.BATTERY_FULL, r0.Battery);
            var steps = stepsDefault;
            while (r0.moveUp())
            {
                steps--;
            }
            if (steps == 0)
            {
                Assert.AreEqual(true, r0.Battery < BaseRobot.MOVEMENT_DELTA_CONSUMPTION);
                r0.recharge();
                Assert.AreEqual(BaseRobot.BATTERY_FULL, r0.Battery);
                steps = stepsDefault;
            }
            else
            {
                Assert.AreEqual(new RobotPosition(0, RobotEnvironment.Y_UPPER_LIMIT), r0.Position);
            }
            while (r0.moveRight())
            {
                steps--;
            }
            if (steps == 0)
            {
                Assert.AreEqual(true, r0.Battery < BaseRobot.MOVEMENT_DELTA_CONSUMPTION);
                r0.recharge();
                Assert.AreEqual(BaseRobot.BATTERY_FULL, r0.Battery);
                steps = stepsDefault;
            }
            else
            {
                Assert.AreEqual(
                    new RobotPosition(RobotEnvironment.X_UPPER_LIMIT, RobotEnvironment.Y_UPPER_LIMIT),
                    r0.Position);
            }
            while (r0.moveRight())
            {
                steps--;
            }
            if (steps == 0)
            {
                Assert.AreEqual(true, r0.Battery < BaseRobot.MOVEMENT_DELTA_CONSUMPTION);
                r0.recharge();
                Assert.AreEqual(BaseRobot.BATTERY_FULL, r0.Battery);
                steps = stepsDefault;
            }
            else
            {
                Assert.AreEqual(
                    new RobotPosition(RobotEnvironment.X_UPPER_LIMIT, RobotEnvironment.Y_UPPER_LIMIT),
                    r0.Position);
            }
        }
    }
}
